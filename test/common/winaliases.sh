# See: https://github.com/bats-core/bats-core/issues/259

# alias gcloud='gcloud.cmd'
gcloud() {
  gcloud.cmd $@
}

# alias gsutil='gsutil.cmd'
gsutil() {
  gsutil.cmd $@
}

# alias sikuli='java -jar /c/SikuliX/sikulixide-2.0.4.jar'
sikulix() {
  java -jar /c/SikuliX/sikulixide-2.0.4.jar $@
}

# alias composite='/c/ImageMagick/composite'
composite() {
  /c/ImageMagick/composite $@
}

# alias identify='/c/ImageMagick/identify'
identify() {
  /c/ImageMagick/identify $@
}
