#!/usr/bin/env bats

source test/common/winaliases.sh
mkdir -p result
mkdir -p resources/1-2/
mkdir -p resources/1-2/output
gsutil -m cp -r gs://fiot-arch-dev-biz-cicd-testdata/biz-app/trial/try-runner-ita/resources/1-2/expected resources/1-2/

# setup() {
# }

# teardown() {
# }

@test "[1-2-1] 期待する時間内に対象画面にアクセスできること" {
  run sikulix -r demo1.sikuli
  [ "$status" -eq 0 ]
  cp result/* resources/1-2/output/
}

@test "[1-2-2] 対象画面の表示が期待通りであること" {
  composite -compose difference resources/1-2/expected/demo1.png resources/1-2/output/demo1.png resources/1-2/output/diff.png
  identify -format "%[mean]" resources/1-2/output/diff.png > resources/1-2/output/identify_output.txt
  DIFF=$(identify -format "%[mean]" resources/1-2/output/diff.png | awk '{print int($1)}')
  [ "$DIFF" -eq 0 ]
}
